package com.example.myapplication;

import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.Serializable;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView maListe = findViewById(R.id.listView);
        final AnimalList animalL = new AnimalList();

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>( this, android.R.layout.simple_list_item_1, animalL.getNameArray());
        maListe.setAdapter(adapter);

        maListe.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {

                final String animalName = (String) parent.getItemAtPosition(position);
                Animal animal = animalL.getAnimal(animalName);
                Intent intent = new Intent(MainActivity.this, AnimalActivity.class);

                intent.putExtra("animalName", animalName);

                startActivity(intent);
                //Toast.makeText(MainActivity.this, "You selected: " + animalName, Toast.LENGTH_LONG).show();

            }
        });
    }
}

