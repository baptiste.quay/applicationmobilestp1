package com.example.myapplication;

import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class AnimalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);

        AnimalList animalList = new AnimalList();
        String name;

        Bundle extras = getIntent().getExtras();
        name = extras.getString("animalName");
        final Animal animal = animalList.getAnimal(name);

        final TextView animalNameTV = (TextView) findViewById(R.id.animalName);
        animalNameTV.setText(name);

        ImageView imageV = findViewById(R.id.imageA);
        String adresse = "@drawable/"+animal.getImgFile();
        int imageRessource = getResources().getIdentifier(adresse, null, getPackageName());
        Drawable res = getResources().getDrawable(imageRessource);
        imageV.setImageDrawable(res);

        final TextView esperanceTV = (TextView) findViewById(R.id.esperanceValue);
        esperanceTV.setText(animal.getHightestLifespan()  + " années");

        final TextView gestationTV = (TextView) findViewById(R.id.gestationValue);
        gestationTV.setText(animal.getGestationPeriod() + " jours");

        final TextView poidsNTV = (TextView) findViewById(R.id.poidsNValue);
        poidsNTV.setText(animal.getBirthWeight() + " kg");

        final TextView poidsATV = (TextView) findViewById(R.id.poidsAValue);
        poidsATV.setText(animal.getAdultWeight() + " kg");

        final TextView conservationStatusValueTV = findViewById(R.id.conservationStatusValue);
        conservationStatusValueTV.setText(animal.getConservationStatus());

        final Button button = findViewById(R.id.save);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                animal.setConservationStatus(conservationStatusValueTV.getText().toString());
            }
        });

    }
}
